import { TestBed, async } from "@angular/core/testing";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgxCoolDialogsModule } from "@csiro-geoanalytics/ngx-cool-dialogs";

import { AppComponent } from "./app.component";
import { DynamicTextComponent } from "./dynamic-text/dynamic-text.component";


describe("AppComponent", () =>
{
	beforeEach(async(() =>
	{
		TestBed.configureTestingModule({
			imports: [
				BrowserModule,
				BrowserAnimationsModule,
				NgxCoolDialogsModule.forRoot()
			],
			declarations: [
				AppComponent,
				DynamicTextComponent
			],
		}).compileComponents();
	}));

	it("should create the app", async(() =>
	{
		const fixture = TestBed.createComponent(AppComponent);
		const app = fixture.debugElement.componentInstance;
		expect(app).toBeTruthy();
	}));

	it("should have as title 'app'", async(() =>
	{
		const fixture = TestBed.createComponent(AppComponent);
		const app = fixture.debugElement.componentInstance;
		expect(app.title).toEqual("app");
	}));

	it("should render title in a H1 tag", async(() =>
	{
		const fixture = TestBed.createComponent(AppComponent);
		fixture.detectChanges();
		const compiled = fixture.debugElement.nativeElement;
		expect(compiled.querySelector("h1").textContent).toContain("Ngx Cool Dialogs");
	}));

	it("should have three themes defined", async(() =>
	{
		const fixture = TestBed.createComponent(AppComponent);
		const app = fixture.debugElement.componentInstance;
		expect(app.themes.length).toEqual(3);
	}));

});
