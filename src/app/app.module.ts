import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgxCoolDialogsModule } from "ng-lib";

import { AppComponent } from "./app.component";
import { DynamicTextComponent } from "./dynamic-text/dynamic-text.component";


@NgModule({
	declarations: [
		AppComponent,
		DynamicTextComponent
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		NgxCoolDialogsModule.forRoot()
	],
	bootstrap: [ AppComponent ]
})
export class AppModule {}
