/*
 * Public API Surface of ng-lib.
 */

export * from "./lib/cool-dialogs.module";
export * from "./lib/cool-dialog";
export * from "./lib/cool-dialogs.config";
export * from "./lib/cool-dialogs.service";
export * from "./lib/cool-dialogs.module";
export * from "./lib/cool-dialogs-types";
export * from "./lib/cool-dialogs-theme";
